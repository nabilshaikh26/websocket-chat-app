var express = require('express');
var socket = require('socket.io');

// Express app setup
var app = express();
var server = app.listen(4000, function(){
    console.log("Listening to requests on port 4000...");
});

// Static files. Here, express app is serving all files in browser as provided in public folder
app.use(express.static('public'));

// Make websocket connection on server side
var io = socket(server);

io.on('connection', function(socket){
    console.log("Socket connection has been established...", socket.id);

    // This will listen to all chat messages being sent by client
    socket.on('chat', function(data){
        io.sockets.emit('chat', data); // Send back the same chat data to all possible clients. Here, io.sockets refers to all possible sockets available
     });

     // This will listen to typing action
    socket.on('typing', function(data){
        socket.broadcast.emit('typing', data)
    });
});