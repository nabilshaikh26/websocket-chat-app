# websocket-chat-app

This is a sample real-time chat app using [express](https://expressjs.com/) and [socket.io](https://socket.io/)

To run it yourself:

1. npm install
2. npx nodemon